#!/usr/bin/python
import os, sys
import logging.config
from pyramid.paster import get_app

virtenv = os.environ["OPENSHIFT_PYTHON_DIR"] + "/virtenv/"
virtualenv = os.path.join(virtenv, "bin/activate_this.py")
try:
    execfile(virtualenv, dict(__file__=virtualenv))
except IOError:
    pass

here = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, os.path.join(here, "pyramidapp"))
config = os.path.join(here, "production.ini")

logging.config.fileConfig(config)

application = get_app(config, 'main')

if __name__ == '__main__':
    from wsgiref.simple_server import make_server
    httpd = make_server('localhost', 8051, application)
    httpd.handle_request()

